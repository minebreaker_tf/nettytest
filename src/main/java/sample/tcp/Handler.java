package sample.tcp;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;

import java.nio.charset.StandardCharsets;

public class Handler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        ByteBuf in = (ByteBuf) msg;
        String received = in.toString(StandardCharsets.UTF_8);
        System.out.println(received);
        ReferenceCountUtil.release(msg);

        String ret = received.startsWith("GET / HTTP") ?
                "HTTP/1.0 200 OK\nContent-Type: text/html\n\n<h1>hello, netty!</h1>" :
                "HTTP/1.0 404 NOT FOUND\nContent-Type: text/html\n\n<h1>404 Not Found</h1>";

        ByteBuf buf = ctx.alloc().buffer();
        buf.writeCharSequence(ret, StandardCharsets.UTF_8);
        ChannelFuture future = ctx.writeAndFlush(buf);
        future.addListener(new ChannelFutureListener() {
            @Override public void operationComplete(ChannelFuture future) throws Exception {
                ctx.close();
            }
        });
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        ctx.close();
    }

}
