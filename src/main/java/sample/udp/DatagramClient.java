package sample.udp;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.DatagramPacket;
import io.netty.channel.socket.nio.NioDatagramChannel;

import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;

public class DatagramClient {

    public static void main(String[] args) throws Exception {

        EventLoopGroup workerGroup = new NioEventLoopGroup();

        try {
            Bootstrap b = new Bootstrap();
            b.group(workerGroup)
             .channel(NioDatagramChannel.class)
             .handler(new SimpleChannelInboundHandler<DatagramPacket>() {
                          @Override
                          protected void channelRead0(ChannelHandlerContext ctx, DatagramPacket msg) throws Exception {
                              // N/A
                          }
                      }
             );

            Channel ch = b.bind(0).sync().channel();
            ch.writeAndFlush(new DatagramPacket(
                    Unpooled.copiedBuffer("hello netty via udp", StandardCharsets.UTF_8),
                    new InetSocketAddress("localhost", 62425)
            )).sync();

        } finally {
            workerGroup.shutdownGracefully();
        }
    }

}
